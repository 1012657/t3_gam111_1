﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePickUp : MonoBehaviour
{
    public int ScoreIncrease;
    public PlayerScoreHolder playerScoreHolder;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerScoreHolder.Score += ScoreIncrease;
            Destroy(gameObject);
        }
    }

}
