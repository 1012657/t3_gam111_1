﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missel : MonoBehaviour
{
    public GameObject target;
    public Rigidbody2D myRB;
    public Vector2 DirToTarget;
    public float moveSpeed;

    // Use this for initialization
    void Start()
    {
        myRB = this.GetComponent<Rigidbody2D>(); //get the players rigidbody
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            DirToTarget = target.transform.position - transform.position;
            transform.right = DirToTarget;
            myRB.velocity = DirToTarget.normalized * moveSpeed;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        Destroy(gameObject);
    }
}
