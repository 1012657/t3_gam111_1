﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
[CreateAssetMenu]
public class WeaponHandler : ScriptableObject
{ 
    [Tooltip("default project type")]
    public Rigidbody2D bulletPrefab; //a normal bullet
    public Rigidbody2D misselPrefab; // a missel
    public int BulletForce; //the amount of force the bullet is being pushed with
    public float BulletSpawnDis; //the disance away from the sorce that the bullet is being pushed with

    public float AOErange;
    public int AOEdamage;
    public GameObject AOEeffect;

    //functions
    public void normalShoot(Transform MyTrans, Vector2 direction) //the function that handels fiering a normal gun 
    {
        Rigidbody2D newBullet;
        newBullet = Instantiate(bulletPrefab, MyTrans.position, MyTrans.rotation);
        newBullet.transform.position += (new Vector3 (direction.x, direction.y, 0)).normalized * BulletSpawnDis;
        newBullet.transform.Rotate(Vector3.forward * -90); //rotate another 90 deg becuase idk unity is wird 
        newBullet.AddForce(direction.normalized * BulletForce); //push the bullet down the fiering direction
        Destroy(newBullet.gameObject, 2); //destroy the bullet*/
    }

    public void TrackingShoot(Transform MyTrans, GameObject Target)
    {
        Vector2 DirToshooter = new Vector2(Target.transform.position.x - MyTrans.position.x, Target.transform.position.y - MyTrans.position.y);
        DirToshooter = DirToshooter.normalized;
        Rigidbody2D NewBullet; //make a new rigid  body for the bullet
        NewBullet = Instantiate(misselPrefab); //make it a coppy of the bullet prefab
        NewBullet.transform.position = new Vector3(MyTrans.transform.position.x + DirToshooter.x * BulletSpawnDis, MyTrans.transform.position.y + DirToshooter.y * BulletSpawnDis, 0); //spawn the bullet "x" units away from me in the fiering direction
        NewBullet.GetComponent<Missel>().target = Target;

    }
    public void AOEattack(Vector3 origen)
    {   
        List<GameObject> enemylist = GameObject.FindGameObjectWithTag("EnemyMan").GetComponent<EnemyManMan>().enemyList;
        GameObject AOEpartical = Instantiate(AOEeffect);
        AOEpartical.transform.position = origen;
        for (int i = 0; i < enemylist.Count; i++)
        {
            if ((enemylist[i].transform.position - origen).magnitude <= AOErange)
                enemylist[i].GetComponent<EnemyMan>().curHealth -= AOEdamage;

        }
    }
}