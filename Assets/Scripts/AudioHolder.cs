﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioHolder : ScriptableObject
{
    public AudioClip[] clips;
    [Range(0.0f, 1.0f)]
    public float minVol = 1, maxVol = 1;
    [Range(-3.0f, 3.0f)]
    public float minPitch = 1, maxPitch = 1;

    public void Play(AudioSource source)
    {
        var clip = clips[Random.Range(0, clips.Length)];

        var curVol = Random.Range(minVol, maxVol);
        var curPitch = Random.Range(minPitch, maxPitch);
        source.clip = clip;
        source.pitch = curPitch;
        source.volume = curVol;
        source.PlayOneShot(clip);

    }
}
