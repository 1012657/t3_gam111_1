﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3ExplsionHandeler : MonoBehaviour {
    public bool canExplode = false;
    PlayerHealtHandeler playerHealtHandeler;
    int Damage;
	// Use this for initialization
	void Start () {
        playerHealtHandeler = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMan>().playerHealtHandeler;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            playerHealtHandeler.curHealth -= Damage;
        canExplode = true;
    }
}
