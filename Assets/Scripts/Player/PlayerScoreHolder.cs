﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerScoreHolder : ScriptableObject {

    public int Score = 0;
}
