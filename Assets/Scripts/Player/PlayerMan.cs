﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerMan : MonoBehaviour
{
    public PlayerMove playerMove;
    public WeaponHandler weaponHandler;
    public PlayerHealtHandeler playerHealtHandeler;
    public PlayerScoreHolder playerScoreHolder;
    public Text scoreUI;
    public Text hpUI;

    Vector2 MouseDir = Vector2.zero;
    Vector3 MousePos;

    // Use this for initialization
    void Start()
    {
        playerScoreHolder.Score = 0;
        playerHealtHandeler.curHealth = playerHealtHandeler.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        //shooting
        //testing for keybord inputs relayting to the fire buttions
        if (Input.GetKeyDown(KeyCode.Mouse0)) //if the user presses mouse one
            weaponHandler.normalShoot(transform, MouseDir); //fire a bullet
        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            var EnemyList = GameObject.FindGameObjectWithTag("EnemyMan").GetComponent<EnemyManMan>().enemyList;

            GameObject ClosestEnemy = EnemyList[0];
            for(int i = 1; i < EnemyList.Count; i++)
            {
                if ((EnemyList[i].transform.position - transform.position).magnitude > (ClosestEnemy.transform.position - transform.position).magnitude)
                    ClosestEnemy = EnemyList[i];
            }
            weaponHandler.TrackingShoot(transform, ClosestEnemy);
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            weaponHandler.AOEattack(transform.position);
        }


        //movement
        //testing for keypresses relating to movement directions
        if (Input.GetKey(KeyCode.W)) //if the player is pressing W
            playerMove.MoveUp(); //run the MoveUp function from the playerMove SO
        if (Input.GetKey(KeyCode.S)) //if the player is pressing S
            playerMove.MoveDown(); //run the MoveDown function from the playerMove SO
        if (Input.GetKey(KeyCode.A)) //if the player is pressing A
            playerMove.MoveLeft(); //run the MoveLeft function from the playerMove SO
        if (Input.GetKey(KeyCode.D)) //if the player is pressing D
            playerMove.MoveRight(); //run the MoveRight function from the playerMove 

        playerMove.MoveResolve(this.GetComponent<Rigidbody2D>());

        //make the player face the mouse
        PlayerFaceMouse(); //call the face mouce functoiun
        scoreUI.text = "Player score: " + playerScoreHolder.Score.ToString();
        hpUI.text = "HP: " + playerHealtHandeler.curHealth.ToString();

        if (playerHealtHandeler.curHealth <= 0)
            SceneManager.LoadScene("hightscore");

    }

    void PlayerFaceMouse()
    {
        MousePos = Input.mousePosition; //input the ouuce position in screen pos
        MousePos = Camera.main.ScreenToWorldPoint(MousePos); //convert from screan space to game space

        MouseDir = new Vector3(MousePos.x - transform.position.x, MousePos.y - transform.position.y, 0); //make a vector that holds the directions from them to the mouse 
        transform.up = MouseDir;

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Laser" || collision.gameObject.tag == "e3")
            playerHealtHandeler.curHealth--;
        else if (collision.gameObject.tag == "LaserTracking")
            playerHealtHandeler.curHealth -= 2;
    }
}
