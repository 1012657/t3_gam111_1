﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerMove : ScriptableObject
{

    //variabels
    public int MoveSpeed; //the players move speed
    public Vector2 MovementVector = Vector2.zero; //the vector that containes the direction the player is moving in
    Vector2 LastMove; //the movement vector form the last frame
    public float MoveDecrease; //the amount Movement scals down between frames when no inputs are applyed

    //functions
    public void MoveUp() //modify the movement vector if the player wishes to move up
    {
        MovementVector += Vector2.up;
    }
    public void MoveDown() //modify the movement vector if the player wishes to move down
    {
        MovementVector += Vector2.down;
    }
    public void MoveLeft() //modify the movement vector if the player wishes to move left
    {
        MovementVector += Vector2.left;
    }
    public void MoveRight() //modify the movement vector if the player wishes to move right
    {
        MovementVector += Vector2.right;
    }
    public void MoveResolve(Rigidbody2D PlayerRB) // resolve the player movement this frame
    {
        
        if (MovementVector != Vector2.zero)
        {

            MovementVector = MovementVector.normalized; //normilise the players movement as to not have them move faster on diagonals
            MovementVector *= MoveSpeed; //scale the movement vector to have a magnitude eqal to the movement speed 
            PlayerRB.velocity = MovementVector; //set the players veloicity equal the the movement vector
            LastMove = MovementVector * MoveDecrease; //scale down thair movement vector and set that equial to their last movement
            MovementVector = Vector2.zero; //reset the movement vetor to hold no directoin
        }
        else if (LastMove != Vector2.zero)
        {
            PlayerRB.velocity = LastMove; //set the players veloicity equal the their last move slightly scaled down
            LastMove *= MoveDecrease; //scale down thair last movement
        }
    }
}
