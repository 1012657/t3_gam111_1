﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int healthRestore;
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {       
            if (collision.gameObject.GetComponent<PlayerMan>().playerHealtHandeler.curHealth + healthRestore< collision.gameObject.GetComponent<PlayerMan>().playerHealtHandeler.maxHealth)
                collision.gameObject.GetComponent<PlayerMan>().playerHealtHandeler.curHealth += healthRestore;
            else
                collision.gameObject.GetComponent<PlayerMan>().playerHealtHandeler.curHealth = collision.gameObject.GetComponent<PlayerMan>().playerHealtHandeler.maxHealth;           
            Destroy(gameObject);
        }
    }
}
