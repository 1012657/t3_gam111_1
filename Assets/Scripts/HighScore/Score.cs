﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public PlayerScoreHolder playerScoreHolder;
    const string DefaultFileName = "defaultScores.json";

    [System.Serializable]
    public class ScoreEntry
    {
        public string name;
        public int score;
    }
    public List<ScoreEntry> EntryList;

    public void addScore()
    {
        ScoreEntry newScore = new ScoreEntry();

        newScore.score = playerScoreHolder.Score;
        //add name

        if (EntryList.Count < 10)
            EntryList.Add(newScore);
        else
        {
            if(newScore.score >= EntryList[9].score)
            {
                EntryList[9] = newScore;
            }
        }

        OnDisable();
    }

    private void OnEnable()
    {
        //stream  it in

        if (System.IO.File.Exists(Application.persistentDataPath + DefaultFileName))
        {
            var jsonString = System.IO.File.ReadAllText(Application.persistentDataPath + DefaultFileName);
            JsonUtility.FromJsonOverwrite(jsonString, this);
        }
    }

    private void OnDisable()
    {
        //stream it out

        var jsonString = JsonUtility.ToJson(this);
        System.IO.File.WriteAllText(Application.persistentDataPath + DefaultFileName, jsonString);
    }


}
