﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
/// <summary>
/// Container for a list of highscores
/// </summary>
public class ScoreList
{
    const string DefaultFileName = "defaultScores.json";

    [System.Serializable]
    public class ScoreEntry
    {
        public string name;
        public int score;

        public ScoreEntry() { }
        public ScoreEntry(string name, int score)
        {
            this.name = name;
            this.score = score;
        }
    }

    public Text[] ScoreArray;
    public List<ScoreEntry> entries = new List<ScoreEntry>();

    public int ExistingScore(string name)
    {
        return entries.FindIndex(x => x.name == name);
    }

    //puts all entries in the correct order from highest to lowest score
    public void Sort()
    {
        entries = entries.OrderByDescending(x => x.score).ToList();
    }

    //sort and then drop scores that are below x on the board
    public void LimitScoreBoardTo(int x = 10)
    {
        Sort();

        if (entries.Count > x)
        {
            entries = entries.GetRange(0, x);
        }
    }

    //save this object out to the default json file
    public void SaveToDefault()
    {
        var jsonString = JsonUtility.ToJson(this);
        System.IO.File.WriteAllText(Application.persistentDataPath + DefaultFileName, jsonString);
    }

    //override this object with the data found in the default json file
    public void LoadFromDefault()
    {
        if (System.IO.File.Exists(Application.persistentDataPath + DefaultFileName))
        {
            var jsonString = System.IO.File.ReadAllText(Application.persistentDataPath + DefaultFileName);
            JsonUtility.FromJsonOverwrite(jsonString, this);
        }
    }
}
