ScoreList holds data for a highscore list.
It can sort and be asked if scores exist for a name if you want only 1 score per person.
It can load and save to a default location in persist storage, this can be changed to custom location easily.

This would form the data backbone for a highscore table. You would then want a highscore gui that you put the scores into for users to see.

When a score is ahceived in your game you would then at end of game check the score and possible name against existing ScoreList entries to determine if it is a new highscore or a new personal best etc.

If you are not maintaining unique name entries then doing something as simple as adding new score entry with something like the example "rawScoreList.entries.Add(new ScoreList.ScoreEntry("Example", 100));" and then use the limitscoresto function to drop the lowest score off the board.