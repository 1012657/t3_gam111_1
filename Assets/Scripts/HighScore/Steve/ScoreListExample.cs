﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simple sample showing how the scorelist exists in the inspector
/// 
/// Uses Scorelist inbuilts to load and save itself to disk
/// </summary>
public class ScoreListExample : MonoBehaviour
{
    public Text scoretable;
    public PlayerScoreHolder playerScoreHolder;

    public ScoreList rawScoreList = new ScoreList();

    public void DropExtraScores()
    {
        rawScoreList.LimitScoreBoardTo();
    }

    private void OnEnable()
    {
        rawScoreList.LoadFromDefault();

        LoadList();


    }


    private void OnDisable()
    {
        rawScoreList.SaveToDefault();
    }

    void onButtionDown()
    {
        rawScoreList.entries.Add(new ScoreList.ScoreEntry("Example", playerScoreHolder.Score));
    }
    public void LoadList()
    {
        for(int i = 0; i < rawScoreList.entries.Count; i++)
        {
            scoretable.text = scoretable.text + "\n";

            scoretable.text += rawScoreList.entries[i].score + rawScoreList.entries[i].name;
        }
    }
}
