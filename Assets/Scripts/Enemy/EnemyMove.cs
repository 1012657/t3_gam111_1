﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyMove : MonoBehaviour
{
    public float moveSpeed;
    public float MinDistanceFromplayerToMove;

    public Rigidbody2D myRB;
    public GameObject Player;

    public Vector3 playerDir;
    // Use this for initialization
    void Start()
    {
        if (myRB == null)
        {
            myRB = this.GetComponent<Rigidbody2D>();
        }

        if (Player == null)
        {
            Player = GameObject.FindGameObjectWithTag("Player");
        }
    }

    // Update is called once per frame
    void Update()
    {
        playerDir = (Player.transform.position - this.transform.position);
        if (playerDir.magnitude >= MinDistanceFromplayerToMove)
            myRB.velocity = ((new Vector2(playerDir.x, playerDir.y)).normalized * moveSpeed);
        else
            myRB.velocity = Vector2.zero;

        transform.up = new Vector2(playerDir.x, playerDir.y);
    }
}
