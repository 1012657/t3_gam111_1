﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMan : MonoBehaviour
{
    public int maxHealth;
    public int curHealth;

    public WeaponHandler weaponHandler;
    public AudioHolder audioHolder;

    public GameObject player;
    public int gunType; //0 for no shooting, 1 for normal, 2 for tracking
    Vector3 playerDir;

    public float shootCountDownTime;
    float shootCountDown;
    private AudioSource audioSource;
    // Use this for initialization
    void Start()
    {
        shootCountDown = shootCountDownTime;
        player = GameObject.FindGameObjectWithTag("Player");
        curHealth = maxHealth;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        playerDir = (player.transform.position - transform.position).normalized;
        if (gunType == 1)
        {
            if (shootCountDown <= 0)
            {
                weaponHandler.normalShoot(transform, this.GetComponent<EnemyMove>().playerDir);
                shootCountDown = shootCountDownTime;
                audioHolder.Play(audioSource);
            }
        }
        else if (gunType == 2)
        {
            if (shootCountDown <= 0)
            {
                weaponHandler.TrackingShoot(transform, player);
                shootCountDown = shootCountDownTime;
            }
        }
        shootCountDown -= Time.deltaTime;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Laser")
            curHealth--;
        else if (collision.gameObject.tag == "LaserTracking")
            curHealth -= 2;
    }
}
