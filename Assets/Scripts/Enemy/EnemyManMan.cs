﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManMan : MonoBehaviour
{
    public float xMax;
    public float yMax;
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject enemy3;

    public GameObject deathParticals;
    public PlayerScoreHolder playerScoreHolder;

    public List<GameObject> enemyList = new List<GameObject>();
    int waveNum = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = enemyList.Count - 1; i >= 0; i--)
            if (enemyList[i].tag == "e3" && enemyList[i].GetComponent<Enemy3ExplsionHandeler>().canExplode == true)
            {
                Debug.Log(enemyList[i].name);
                GameObject newExplosion = Instantiate(deathParticals);
                newExplosion.transform.position = enemyList[i].transform.position;
                Destroy(newExplosion, 1f);
                Destroy(enemyList[i]);
                enemyList.RemoveAt(i); 
                playerScoreHolder.Score += 10;
            }
            else if (enemyList[i].GetComponent<EnemyMan>().curHealth <= 0)
            {
                Debug.Log(enemyList[i].name);
                GameObject newExplosion = Instantiate(deathParticals);
                newExplosion.transform.position = enemyList[i].transform.position;
                Destroy(newExplosion, 1f);
                Destroy(enemyList[i]);
                enemyList.RemoveAt(i);
                playerScoreHolder.Score += 10;
            }
        if (enemyList.Count == 0)
            waveUp();
    }
    private void waveUp()
    {
        waveNum++;

        for (int i = 0; i <= waveNum * 3; i++)
            spawnEnemy(enemy1);
        for (int i = 0; i <= waveNum * 2; i++)
        {
            Debug.Log("spawning e3");
            spawnEnemy(enemy3);
        }
        for (int i = 0; i <= waveNum * 2; i++)
            spawnEnemy(enemy2);

    }

    void spawnEnemy(GameObject enemy)
    {
        GameObject newEnemy;
        newEnemy = Instantiate(enemy);

        Vector2 newEnemyPos = new Vector2(Random.Range(-xMax, xMax), Random.Range(-yMax, yMax));
        newEnemy.transform.position = newEnemyPos;
        enemyList.Add(newEnemy);
    }
}
